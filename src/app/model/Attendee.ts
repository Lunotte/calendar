import { AttendeeStatus } from './AttendeeStatus';
import { Event } from './Event';
import { Users } from './Users';

export class Attendee{
    id: number;
    user: Users;
    event: Event;
    status : String;
    answerDate : Date;
}