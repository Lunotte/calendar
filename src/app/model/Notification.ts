import { Attendee } from './Attendee';
import { Event } from './Event';

export class Notification{
    id: number;
    status: String;
    title: String;
    description: String;
    firingEvent: Event;
    attendee: Attendee
}