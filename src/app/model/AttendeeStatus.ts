export enum AttendeeStatus{
    PENDING,  
    ACCEPTED,  
    REFUSED
}