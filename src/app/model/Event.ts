
import { Calendar } from './Calendar';
import { EventType } from './EventType';
import { Users } from './Users';
import {JsonProperty} from 'json-typescript-mapper';

export class Event{
    id : number;
    //@JsonProperty('owner')
    owner : Users;
   // @JsonProperty('name')
    title : String;
    name : String;
    description : String;
    place : String;  
   // @JsonProperty('type')
    type : String; 
   // @JsonProperty('startTime')
    start : Date;
    startTime : Date; 
   // @JsonProperty('endTime')
    end : Date; 
    endTime : Date;
  //  @JsonProperty('cancelDate')
    cancel : Date;
    calendar : Calendar

    /*constructor() {
        this.id = undefined;
        this.owner = undefined;
        this.title = undefined;
        this.description = undefined
        this.place = undefined;
        this.type = undefined;
        this.start = undefined;
        this.end = undefined;
        this.cancel = undefined;
        this.calendar = undefined;
    } */
}