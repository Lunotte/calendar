import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
 
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {deserialize} from 'json-typescript-mapper';
import { SelectItem } from 'primeng/primeng';

import { Calendar } from '../model/Calendar';
import { Event } from '../model/Event';
import { Users } from '../model/Users';

@Injectable()
export class EventService {

    private url = 'http://127.0.0.1:8000/api';  // URL to web API
    
    constructor (private http: Http) {}

    getEvents(): Promise<Event[]> {
        let options = this.optionsHeader();

        return this.http.get(this.url+'/events/', options)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
    }

    getEvent(userId: String): Promise<Event> {
        let options = this.optionsHeader();

        return this.http.get(this.url+'/events/'+userId, options)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
    }


    getEventsForMe(user : String): Observable<Event[]> {
        let options = this.optionsHeader();
    
        return this.http.get(this.url+'/events/users/'+user, options)
                    .map(this.extractData)
                    .catch(this.handleError);
    }

    saveEvent(param: any): Observable<any> {
        let options = this.optionsHeader();
             
       

        console.log(param);
        return this.http.post(this.url+'/events', param, options)
        .map(this.extractData)
                .catch(this.handleError);
         
    }

    updateEvent(event: Event): Observable<any> {
        let options = this.optionsHeader();
        
        return this.http.put(this.url+'/events/'+event.id, {event}, options)
                .map(this.extractData)
                .catch(this.handleError);
    }

    cancelEvent(event: Event): Observable<any> {
        let options = this.optionsHeader();
        
        return this.http.put(this.url+'/events/cancel/'+event.id, {event}, options)
                .map(this.extractData)
                .catch(this.handleError);
    }
    
    private optionsHeader(){
        let data = JSON.parse(localStorage.getItem('currentUser')); 
        let headers = new Headers({ 'Content-Type': 'application/json', 'token': data.token});
                                    
        let options = new RequestOptions({ headers: headers });
        
        return options;
    }

    private extractData(res: Response) {
        let body = res.json();
       console.log(body);

        let events: Event[] = [];
        if(body instanceof Array){
            body.forEach(element => {
                element.start = element.startTime;
                element.end = element.endTime;
                element.title = element.name;
                events.push(element);
            });
            console.log(events);
            return events || { };
        }else{
            body.event.start = body.event.startTime;
            body.event.end = body.event.endTime;
            body.event.title = body.event.name;
            return body.event || { };
        }
               
    }
    
    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }
}

