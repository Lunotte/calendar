import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
 
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Users } from '../model/Users';

@Injectable()
export class UsersService {
  private url = 'http://127.0.0.1:8000/api';  // URL to web API
 
  constructor (private http: Http) {}

  getUsers(): Observable<Users[]> {
    let options = this.optionsHeader();

    return this.http.get(this.url+'/users', options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getUser(id: string): Observable<Users> {
    let options = this.optionsHeader();
    
    return this.http.get(this.url+'/users/'+id, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private optionsHeader(){
    let data = JSON.parse(localStorage.getItem('currentUser')); 
    let headers = new Headers({ 'Content-Type': 'application/json', 'token': data.token});
                                
    let options = new RequestOptions({ headers: headers });

    return options;
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log(body);

    return body || { };
  }
  
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';

      const err = body.error || JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}

