import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
 
@Injectable()
export class AuthenticationService {
    public token: string;
    public refreshToken: string;
    public requestOk: boolean;
 
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
 
    login(username: string, password: string): Observable<boolean> {
        let param = {
                        "username": username,
                        "password": password
                    }
        let headers = new Headers({ 'X-Requested-With': 'XMLHttpRequest',
                                    'Content-Type': 'application/json',
                                    'Cache-Control': 'no-cache' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://beaugrand.in:9966/api/auth/login', param, options)
            .map((response: Response) => {

                this.token = response.json().token;
                this.refreshToken = response.json().refreshToken;

                this.requestOk = (this.token) ? true : false;
                console.log(this.requestOk);
                
                if(this.requestOk){
                    // store userid and token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ refreshToken: this.refreshToken, token: this.token }));
                    // return true to indicate successful login*/
                    return true;
                }else{
                    return false;
                }
               
            })
           // .catch(this.handleError);
    }
 
    logout(): void {
        let data = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Content-Type': 'application/json'
                                , 'token': data.token
                                , 'userid': data.userid });
        let options = new RequestOptions({ headers: headers });

        this.http.delete('http://127.0.0.1:8000/api/users/logout', options)
            .toPromise()
            .then(this.isOKLogout)
            .catch(this.handleError);


        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }

    private isOKLogout(error: Response | any) {
        
    }

    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
          /*console.log("error : ");      
          console.log(error);*/
          
          const body = error.json() || '';
    
          /*console.log("body : ");      
          console.log(body);*/
          const err = body.error || JSON.stringify(body);
    
         /* console.log("err : ");      
          console.log(err);*/
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
  }
}