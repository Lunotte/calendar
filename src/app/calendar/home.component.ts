import { ChangeDetectorRef, Component, Injectable, OnInit } from '@angular/core';
import { ScheduleModule, SelectItem } from 'primeng/primeng';
import { log } from 'util';

import { AttendeeStatus } from '../model/AttendeeStatus';
import { Calendar } from '../model/Calendar';
import { Event } from '../model/Event';
import { EventType } from '../model/EventType';
import { Login } from '../model/Login';
import { Notification } from '../model/Notification';
import { Users } from '../model/Users';
import { AttendeeService } from '../service/attendee.service';
import { CalendarService } from '../service/calendar.service';
import { EventService } from '../service/event.service';
import { NotificationService } from '../service/notification.service';
import { UsersService } from '../service/users.service';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  providers: [UsersService, EventService,
   CalendarService, NotificationService,
   AttendeeService]
})
export class HomeComponent implements OnInit {

    errorMessage : string;
    
    users: Users[];
    usersList: SelectItem[];
    selectedUsers: string[];
    calendars : Calendar[];
    notifications : any[];
    events: any[] = [];
    evenements: Event[] = [];

    user: Users;
    selectedCalendar : Calendar;
    calendar : Calendar;
    event: Event;
    login: Login;

    header: any;
    
    dialogVisible: boolean = false; 
    dialogEditCalendar: boolean = false;
    dialogError: boolean = false;

    //idGen: number = 100;
    today: Date;
    showCancelButton: boolean;
    me: any;
    
    constructor(private eventService: EventService, private calendarService: CalendarService, 
                private usersService: UsersService, private cd: ChangeDetectorRef,
                private notificationService: NotificationService, private attendeeService: AttendeeService) {
     }

    ngOnInit() {
       /* this.events = [
        {
            "id": 1,
            "title": "First Event",
            "description": "Procédure pour virer Jean Marc",
            "place": "Mon bureau",
            "type": "ALL_DAY",
            "start": "2017-06-27T12:44:51+02:00",
            "end": "2017-06-28T10:17:15+02:00",
            "cancelDate": null,
            "calendar": {
                "id": 2,
                "name": "mon calendar",
                "privat": true
            }
        },
        {
            "id": 2,
            "title": "Second Event",
            "description": "Débat sur l'utilité de Jean Marc",
            "place": "Salle de conférence",
            "type": "FIXED_DURATION",
            "start": "2017-06-24T08:17:24+02:00",
            "end": "2017-06-27T11:27:28+02:00",
            "cancelDate": null,
            "calendar": {
                "id": 2,
                "name": "mon calendar",
                "privat": true
            }
        }];*/
        
        this.header = {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        };
        
        this.selectedCalendar = null;
        this.calendar = new Calendar();
        this.me = JSON.parse(localStorage.getItem('currentUser'));
       
        this.getEventsForMe(this.me.userid);
        this.getNotifications();
        this.getCalendars();
        this.getUser(this.me.userid);
        this.usersList = [];
        this.getUsers();
    }

    

    
    handleDayClick(event) {
        if(this.selectedCalendar != null){
            this.today = new Date();
        
            this.event = new Event();
            this.event.start = new Date(event.date.format());
            this.showCancel();
            this.dialogVisible = true;
            
            //trigger detection manually as somehow only moving the mouse quickly after click triggers the automatic detection
            this.cd.detectChanges();
        }else{
           // console.log("pas de calendar de selectionné");
            this.showDialogError("Pas de calendrier sélectionné.");
        }
    }
    
    handleEventClick(e) {
     
            this.event = new Event();
            this.event.title = e.calEvent.title;
            this.event.type = e.calEvent.type;
            this.event.place = e.calEvent.place;
            this.event.description = e.calEvent.description;
            this.event.owner = e.calEvent.owner;
                    
            let start = e.calEvent.start;
            let end = e.calEvent.end;
        
            if(end) {
                this.event.end = new Date(end.format());
            }
    
            this.event.id = e.calEvent.id;
            this.event.start = new Date(start.format());
            
            this.dialogVisible = true;
        
    }

    showCancel() :void{
        if(this.event.start > this.today){
            this.showCancelButton = true;
        }else{
            this.showCancelButton = false;
        }
        
    }

    loadCalendar(m): void{
        
        if(m != null && m.id != null){
            this.evenements = [];
            this.selectedCalendar = m;
            this.events.forEach(element => {
                if(element.calendar.id == m.id){
                    this.evenements.push(element);
                }
            });
        }
    }
    
    saveEvent() {
                
        //update
        if(this.event.id) {
            let index: number = this.findEventIndexById(this.event.id);
            if(index >= 0) {
                this.events[index] = this.event;
            }
        }
        //new
        else {
            if(this.event.end) {                
                if(this.event.start === this.event.end){
                    this.event.type = EventType[EventType.ALL_DAY];
                }else{
                    this.event.type = EventType[EventType.FIXED_DURATION];
                }
            }else{
                this.event.type = EventType[EventType.ALL_DAY];
            }

            this.event.start = this.event.start       
            this.event.owner = this.user;
            this.events.push(this.event);
              
            this.actionSaveEvent();
            this.event = null;
        }
        
        this.dialogVisible = false;
    }

    actionSaveEvent(): void{
         let param = {
            "event":{
                "calendar":this.selectedCalendar.id,   
                "name": this.event.title,
                "description": this.event.description,
                "place": this.event.place,
                "type": this.event.type,
                "startTime": this.event.start,
                "endTime": this.event.end,
                "cancelDate": ""
            },
            "attendee":this.selectedUsers
        };
        this.eventService.saveEvent(param).subscribe(
            (p)=>{
                this.evenements.push(p);
            });

    }
    
    deleteEvent() {
        let index: number = this.findEventIndexById(this.event.id);
        if(index >= 0) {
            this.events.splice(index, 1);
        }
        this.dialogVisible = false;
    }

    cancelEvent(){
        this.event.cancel = new Date();
        this.actionCancelEvent();
        this.dialogVisible = false;
    }

    actionCancelEvent(){
        this.eventService.cancelEvent(this.event)
                        .subscribe(
                                 (param) => {
                            this.getEventsForMe(this.me.userid);                          
                        },
                             error =>  this.errorMessage = <any>error
                         );
    }

    cancelCalendar(){
        this.dialogEditCalendar = false;
    }
    
    findEventIndexById(id: number) {
        let index = -1;
        for(let i = 0; i < this.events.length; i++) {
            if(id == this.events[i].id) {
                index = i;
                break;
            }
        }
        
        return index;
    }


    showDialogEditCalendar(){
        this.dialogEditCalendar = true;
    }

    showDialogError(message: string){
        this.errorMessage = message;
        this.dialogError = true;
    }

    okError(){
        this.dialogError = false;
    }

    saveCalendar(){
        if(this.calendar.privat === undefined){
            this.calendar.privat = false;
        }
        this.actionSaveCalendar();
        this.calendar = new Calendar();
        this.dialogEditCalendar = false;
    }

    actionSaveCalendar(): void{
        this.calendarService.saveCalendar(this.calendar)
                        .subscribe(
                        (param) => {
                            this.calendars.push(param);                           
                        });

    }

    notificationAccept(notification: Notification){
        notification.attendee.status = AttendeeStatus[AttendeeStatus.ACCEPTED];

        this.actionNotificationRead(notification);
        this.actionNotificationAttendee(notification);
    }

    notificationRefuse(notification: Notification){
        notification.attendee.status = AttendeeStatus[AttendeeStatus.REFUSED];
    
        this.actionNotificationRead(notification);
        this.actionNotificationAttendee(notification);
    }

    actionNotificationRead(notification: Notification){
     this.notificationService.updateNotification(notification)
                        .subscribe(
                            (param) => {
                            this.getNotifications();                          
                        },
                             error =>  this.errorMessage = <any>error
                         );
    }

    actionNotificationAttendee(notification: Notification){ 
        this.attendeeService.updateNotification(notification.attendee)
                            .subscribe(
                             error =>  this.errorMessage = <any>error
                         );
    }
    



    getUsers(): void {
        this.usersService.getUsers()
                        .subscribe(
                        (param) => {
                            param.forEach(element => {
                                this.usersList.push({label: element.firstname+ " "+element.lastname, value: element.id});
                            });
                            
                        });
    }

    getUser(user: string): void {
        this.usersService.getUser(user)
                        .subscribe(
                        user => this.user = user,
                        error =>  this.errorMessage = <any>error);
    }
    
    getEventsForMe(user: String): void {
        this.eventService.getEventsForMe(user)
                        .subscribe(
                            (param) => {
                                param.forEach(element => {
                                    this.events.push(element);
                                });   
                                this.loadCalendar(this.selectedCalendar);                        
                        });
                       // events => this.events = events,
                        //error =>  this.errorMessage = <any>error);
    }

    getCalendars(): void {
        this.calendarService.getCalendars()
                        .then(
                        calendars => this.calendars = calendars,
                        error =>  this.errorMessage = <any>error);
    }

    getNotifications(): void {
        this.notificationService.getNotifications()
                        .subscribe(
                        notifications => this.notifications = notifications,
                        error =>  this.errorMessage = <any>error);
    }
}

