import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
import { AuthenticationService } from '../service/authentification.service';
 
@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})
 
export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
 
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }
 
    ngOnInit() {
        // reset login status
       // this.authenticationService.logout();
    }
 
    login() {
        this.loading = true;
            this.authenticationService.login(this.model.username, this.model.password)
                .subscribe(
                    data => this.redirect(),
                    err => this.noRedirect()
            );
    }

    /**
     * Auth successful
     */
    redirect(){
        this.router.navigate(['/']);
        console.log("Retourne true");
    }

    /**
     * Auth failed
     */
    noRedirect(){
        this.error = 'Username or password is incorrect';
        this.loading = false;
    }
}