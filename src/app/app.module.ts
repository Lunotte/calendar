import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { HomeComponent } from './calendar/home.component';
import { AuthGuard } from './guard/auth.guard';
import { AuthenticationService } from './service/authentification.service';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

import { InputTextModule, ButtonModule, ScheduleModule, DialogModule, 
         CalendarModule, CheckboxModule, ListboxModule, RadioButtonModule}  from 'primeng/primeng';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true }),
    InputTextModule, 
    ButtonModule,
    ScheduleModule,
    DialogModule,
    CalendarModule,
    CheckboxModule,
    ListboxModule,
    RadioButtonModule
  ],
  providers: [AuthGuard,
  AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
